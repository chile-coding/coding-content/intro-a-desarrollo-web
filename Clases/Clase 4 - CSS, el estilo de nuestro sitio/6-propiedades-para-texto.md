# Teoria

## Practica

1. Vamos a ocupar la propiedad `font-size`, ve a tu archivo css y aplica lo siguiente:
    * selecciona al id header y añade un `font-size` de `2em`
    * selecciona al id main y añade un `font-size` de `2em`
    * selecciona al id contact y añade un `font-size` de `2em`
    **Se puede mejorar algo?**
    * Crea una clase llamada `fs-2` y que tenga la propiedad `font-size` con valor `2em`
    * Borra las configuraciones que hiciste a los ids anteriores, y en cambio ve a cada elemento y colocale esta clase en su atributo class.

2. Vamos a ocupar la propiedad `text-transform`, ve a tu archivo css y aplica lo siguiente:
    * Crea la clase de nombre `upper` y coloca `text-transform:uppercase`
    * Luego ve a tu archivo html a la seccion de id `service-list` y coloca esta clase a los elementos `h4` que se encuentran dentro.

3. Vamos a ocupar la propiedad `font-weight`, recuerdas el elemento `<span>`? vamos a seleccionar algunas palabras de nuestro texto entre elementos span, y luego le daremos una clase que las coloca en "negrita". 

    > ¿Por que no directamente colocamos el elemento `<b>`? por que quizas luego nuestro cliente nos pida que ademas las palabras en negrita tengan un color especial. Siempre hay que pensar en que nuestro codigo sea "escalable"

    * Primero crea la clase `bold` y coloca la siguiente configuracion `font-weight:bold`
    * Ve a las siguientes palabras y envuelvelas en un elemento span que tenga la clase `bold` (TIP: Buscalas con Ctr+F):
        * Dentro del id `service-list` el primer parrafo la frase **decades of experience**.
        * En la seccion de id `values`, dentro de la lista desordenada, el ultimo item, las palabras la palabra **best price**.
        * Dentro del elemento `footer` la palabra **love**.

4. Vamos a ocupar la propiedad `line-height`, ve a tu archivo css y aplica lo siguiente:
    * Crea la clase llamada `lh-50` con `line-height:1.5`
    * Ve a la seccion con id `values` y colocale esta clase al elemento `ul`.
    * Listo, espaciara a los elementos de la lista un poco mas.

