# Intro
Teoria...

## Inline styling

Vamos a trabajar sobre el archivo de la clase pasada,¿Si? Vas muy bien y si tienes puntos sin terminar de la clase anterior, en este link encontraras el archivo html listo para continuar. (Pero luego terminalo, ya que las proximas clases no sera asi.)
* Ve al link y ten tu editor de texto abierto con la carpeta de esta clase creada.
* Tipea **Ctrl + U (Cmd + U para los mac)**
* Ve a la ventana con el codigo de fuente y haz **Ctr + A (Cmd + A)** para seleccionar todo
* Luego presiona **Ctr + C (Cmd +C)** para copiar el contenido
* crea un archivo html vacio y con **Ctr + V (Cmd + V)** pega el contenido.
* Ahora guardalo, sabes cual es la combinacion verdad? y abrelo en el navegador.

1. Desde el editor de texto vamos a incluir nuestro primer estilo. Le daremos un color de fondo a una seccion.
    * Ve a la seccion con id="work-steps"
    * Dentro de su opening tag coloca un atributo `style`, recuerdas que el valor de todo atributo va en comillas? bien!
    * En este caso el valor del atributo es una sentencia css, debemos declarar la propiedad `background-color` y a lo que representa.
    * Coloca `background-color:steelblue`
    * Guarda el archivo y abrelo en el navegador!

## Elemento style

1. Ahora pasamos del atributo al elemento, y esto nos permite por primera vez escribir realmente codigo css en nuestro archivo.

    * Borra el atributo style que creaste hace unos minutos junto con su contenido. El color de fondo de la seccion volvera a ser blanco.
    * A la linea 5 del archivo, veras el elemento `title` que esta dentro del elemento `head` 
    * Vamos a crear un **elemento** `style` (No atributo) que sera envuelto por el `head`. Hazlo a continuacion de `title` con `<style></style>`
    * Ahora dentro del elemento style iran nuestras sentencias CSS.
    * Las letras de nuestro archivo tienen un color negro, que no es conveniente desde el punto de vista de la experiencia dle usuario.Vamos a cambiar todas las letras a un color gris oscuro.
    * Como ya no utilizamos un atributo que esta sobre el elemento, nuestro css debe de "seleccionarlo" con un **Selector**
    * Usa el selector de elemento para seleccionar al `body` y darle un `color` `#1c1e21` 
    ```CSS
        body {
            color:#545454
        }
    ```
    * Guarda el archivo y abrelo en el navegador!

## Un archivo CSS

1. Como dentro del elemento style es puramente codigo css, solo debemos moverlo al nuevo archivo.
    *  Selecciona todo el contenido dentro del elemento style
    *  Pegalo en un nuevo archivo dentro de la misma carpeta, que en vez de extension .html tendra .css, llamalo "main.css"
    *  Guarda ambos archivos y abri el html en el navegador para ver que sucedio.
    *  Si no estas familiarizado con los tonos de negro no te daras cuenta, pero tu archivo css no esta funcionando!
    *  Tranquilo, el unico problema es que ambos archivos no estan relacionados, para hacerlo coloca un elemento `link` dentro del `head` de la siguiente manera

    ```HTML
        <head>
            <title>Honesty & Heart</title>
            <link rel="stylesheet" href="main.css">
        </head>
    ```
    * Ahora si, guardalo y ve a verlo!



