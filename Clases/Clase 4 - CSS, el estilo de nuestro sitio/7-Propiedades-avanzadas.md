1. La propiedad `width`:
    * Crear la clase `service-img` y colocarle la propiedad `width` con valor `500px`.
    * Coloca esta clase a las imagenes incluidas dentro de la seccion de id `service-list`

2. La propiedad `box-shadow`:
    * Dentro de la clase `service-img` y colocarle la propiedad `box-shadow` con valor `0px 0px 10px 2px #545454`.
    
3. La propiedad `border-radius`
    * Dentro de la clase `service-img` y colocarle la propiedad `border-radius` con valor `5px`    * 