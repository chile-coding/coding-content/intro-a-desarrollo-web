# Dando estilo a nuestro proyecto
Vamos a trabajar puramente sobre el archivo css que creaste.
Recuerda lo siguiente **Ideas numeradas y clavos en punta** repitelo 3 veces y jamas te olvidaras que:
* Para seleccionar a un elemento por su id colocamos un numeral y el nombre de id *ideas numeradas* `#nombre{...}`
* Para seleccionar a un elemento por su clase colocamos un punto y el nombre de clase *clavos en punta* `.clase{...}`

## Selector de id
Volvamos a colocarle el **color de fondo** a **la seccion work-steps**.
* Necesitas el selector de id `#`
* el nombre del id `work-steps`
* el nombre de la propiedad css `background-color`
* el valor de la propiedad css `steelblue`
* Listo arma tu sentencia CSS, guarda el archivo y abrelo desde el navegador para ver el cambio.

## Selector de clases
Imagina que queremos que todos los titulos de secciones esten centrados. Eso son todos los `h2`,`h3` y los `h4`.
La propiedad se llama `text-align` y el valor que debemos colocarle es `center`.
1.  Te imaginas como hacerlo? podriamos colocar dos selectores de elemento y escribir las sentencias asi:
```CSS
h2 {
    text-align:center
}
h3 {
    text-align:center
}

h4 {
    text-align:center
}
```
Pero estariamos duplicando codigo, y eso no nos gusta. Mejor utilicemos una clase.
* Necesitamos un selector de clase `.`
* Debemos idear el nombre de la clase, por ejemplo `text-center`
* Debemos saber como escribir la sentencia, que lo tenemos mas arriba.
* Crea tu primer clase entonces de la siguiente manera:
```CSS
.text-center{
    text-align:center
}
```
* Ahora ve a todos los `h2` solo uno ,`h3` (son 4) y `h4` (son 2) y colocales el atributo `class` y de valor el nombre de tu clase, quedando `class="text-center"`.
* Guarda ambos archivos y actualiza tu navegador.

## La magia de las clases
Los `h4` son mas pequeños y se utilizan para describir los servicios. Pongamosle una diferenciacion.
*   Crea una nueva clase que se llame `color-secondary`
*   Que la clase configure la propiedad `color` a un valor de `#303237`
*   Ahora ve a los elementos `h4` y dentro del atributo `class` que ya tienen coloca el nombre de esta clase tambien, separada por un espacio. `class="text-center color-secondary"`
* voila! la magia de las clases, colocamos estilo a los elementos sumandolas.

Aprendimos que **se puede tener mas de una clase**, y ademas, si no te has dado cuenta, vemos que **las clases y los ids conviven sin problemas** como por ejemplo en el elemento `h2` de id `about`.