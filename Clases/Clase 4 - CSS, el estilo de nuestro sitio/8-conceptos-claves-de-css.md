# Conceptos
## Herencia
Ya hemos disfrutado de la herencia pero sin darnos cuenta.
En la utilizacion de la propiedad `line-height` por ejemplo se la colocamos al elemento `ul` sin embargo la heredan los elementos `li`.
1. Lo vamos a ver mejor:
    *  Crea una clase de nombre `nav-button` y coloca las siguientes propiedades
        *  `border` de valor `1px solid #2f2f2f`
        *   `border-radius` de valor `20px`
        *   `background-color` de valor `#2f2f2f`
        *   `font-size` de valor `1.5em`
    * Coloca esta clase a los elementos button 
2. Ahora veremos que se esta heredando por los elementos `a` dentro de estos `button`.
    * Crea una clase `nav-link` con las propiedades:
        * `text-decoration` de valor `none`
        * `color` de valor `#f9f9f9`
        * y coloca un `background-color` de valor `red`
        * Colocar esta clase a los elementos `a` dentro de los botones.
        * Abre el archivo en el navegador.
Ahora **remueve la propiedad `background-color` de la clase `nav-link` y vuelve a ver el archivo. Al no especificar esta propiedad la hereda de su elemento padre de clase `nav-button`

## Especificidad
Un ejemplo de especificidad:
* Con un selector de elemento selecciona al elemento `ol` y dale un `text-align` de valor `center`, Abre el archivo en el navegador y veras a la lista de la seccion `work-steps` centrada.
* Luego crea una clase que se llame `text-left` y tenga por reglas `text-align:left`. Coloca esta clase al elemento ol de la seccion `work-steps`.
* Ahora vas a ver que el texto esta a la derecha. ¿Puede ser por que se declaro ultimo?
    * Mueve la seleccion del elemento ol al **final del archivo**, quedando justo despues de la clase `text-left`
    * Ahora vemos que nada a cambiado, que paso? **la clase es mas especifica que la seleccion del elemento.**
* Ahora creemos un selector de **id** de nombre text-right con la regla `text-align:right` y colequemos el id al elemento ol sin borrar la clase.
* Ahora vemos que el Id es mas fuerte por ser mas especifico!

## Importancia
Si al elemento ol lo queremos de todas formas a la izquierda y no podemos borrar el html, ¿Que hacemos?
* anda a la clase text-left que creaste y coloca en la regla css `!important` quedando:
    ```css
    .text-left{
    text-align:left!important}
    ```
* Veras el cambio en tu archivo aun siendo una clase y teniendo selectores mas especificos declarados.
Important fuerza la especificidad para propiedades que queramos que si o si se reescriban.

## Re escribir estilos con una declaracion css
Al final de tu documento vuelve a seleccionar al id `work-steps`, (sin borrar la seleccion que tienes al principio).
Añadele estas reglas css:
*  `text-shadow: 0px 0px 10px #545454;`
*  `color: #f9f9f9;`
Veras que las mismas se aplican al elemento, es decir, podemos volver a nombrar clases, el navegador reunira todas las intrucciones de una clase a medida que fue nombrada y si una propiedad difiere en su valor, el predominante sera el ultimo nombrado.

## Re escribir estilos con otro archivo css
Es sintesis es lo mismo que explicamos anteriormente pero en un nuevo archivo, es decir, si dos archivos tienen declarados mismos nombres de clases. Que estilos son aplicados? el ultimo archivo importado en el navegador (dentro del elemento head)

## Conclusion
Al final de esta clase deberas tener un sitio que [se ve de esta manera][1] y es que te estas volviendo cada vez mas profesional.

[1]:https://tsc-clase-4.netlify.com/ "Sitio terminado"