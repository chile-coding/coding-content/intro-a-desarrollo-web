# Challenge
Ya discutido sobre el contenido y mejorado un poco los estilos, tuvimos a fin de la clase anterior una demo con nuestro cliente. El nos solicito ver cambios que adapten mas a un recorrido del usuario por el sitio.

## Requerimientos
* Al cargar el sitio se debe ver en toda la pantalla, el nombre del estudio, y los botones de navegacion con una imagen representativa.
* El footer debe acompañar al scroll del sitio con un color de fondo que se ajuste a la imagen corporativa
* Prefiere a los servicios, descriptos con sus imagenes en linea con el texto.
* Ademas quiere que una barra e titulo acompañe toda esa seccion indicando que es la servicios.
* Las imagenes deben tener un efecto sombreado que le de mejor estilo .
* La seccion de values tambien debe tener un fondo y no le gusta los bullets de la lista desordenada.
* El formulario de contacto no se decide aun sobre su estilo, quedara para la proxima demo.

Vamos a recorrer nuestro proyecto intentando organizarlo en cada seccion:
Asi que crea la carpeta de esta clase y copia dentro el proyecto con el que veniamos trabajando, tanto el html como el css.
Deberias [tenerlo asi][1] desde la ultima clase

## Aplicar `box-sizing:border-box`
Lo primero que deben hacer es a cada seleccion o elemento aplicarle correctamente la unidad de box sizing
1. Vayan a todas las declaraciones css y coloquen al principio `box-sizing:border-box`
    * Haganlo como programadores, seleccionen un principio de llave "{"
    * Luego presionen Ctr +D (o Cmd + D si estan desde mac) de esta manera se seleccionara todas las repetidas.
    * Luego presiona la flecha derecha, para ubicarte al costado derecho de la llave
    * presiona Enter para saltar la linea.
    * y tipea (o pega) `box-sizing:border-box;`

## Ordenando elementos
Ves una linea blanca que enmarca a nuestro sitio? se notara aun mas en las secciones con fondo de color.
1. Ve al selector del body que esta al principio del css y coloca:
    *  un padding de valor cero en todos sus lados  `padding: 0px;`
    * Un margin de valor cero en todos sus lados   `margin:0px`
2. Vamos a ser ordenados y luego del selector body, sera por la linea 8 aproximadamente del archivo css:
    * Crea un selector del id `header`
    * Colocale el **recuerda el box-sizing**
    * Hace que el **texto** se alinea al **centro**
    * Dale un color de valor `#f9f9f9`
    * Apliquemos el tamaño de fuente con valor `2em` directamente aqui.
    * Demosle un alto de toda la pantalla con la propiedad `height` y el valor `100vh`
    * Por ultimo coloquemos un color de fondo `background-color: #545454cf`
    * Ahora podemos ir al elemento `header` que posee este id y quitarle la clase fs-2 ya que lo incluimos recien.
3. Si vemos nuestro archivo, hay un espacio en blanco al principio, debe ser el margin.
    * Coloquemos el `margin` del `id header` en cero como lo hicimos con `body`
    * Vemos nuestro archivo nuevamente y esto no funciona, ya que no es el margin de header el rebelde, sino el margin del elemento `h1`.
    * Debajo de la seleccion del id header, seleccionemos ahora al id title correspondiente al elemento `h1` y coloquemos la propiedad `margin:0px`
4. Los botones de la barra de navegacion podrian verse mejor
    * ve a la clase nav-button y añadele `padding:10px;`
    * ve a la clase nav-link y añade `padding:20px;`
5. Los servicios:
    * Selecciona al id `service-list` y de reglas css colocale:
        *  Para encogerlo un poco `width:80vw;`
        * `padding: 10px;` Para que no quede tan cercano al borde
        * `margin:0 auto;` Para centrarlo horizontalmente
6. Vamos a crear una clase para el concepto de "servicios",
    * Quita los id de `comercial-service` y `supervision-service` y agrega a dichos divs, un atributo `class="service"`
    * Selecciona la clase service en tu css
    * Dale como unica regla `margin: 10px;`
7. Dentro de los div de clase "service", hay elementos `h4` y `p` que de alguna manera son descripciones verdad?
    * Envuelve el combo **h4+p** de cada div en un nuevo div que tenga una clase "service-description"
    * Que estamos haciendo aca? separando los conceptos, recuerda que un elemento div es como una pecera, que nos permite separar ideas y conjunto de elementos.
    * En tu archivo css crea un selector de clase "service-description" y dale estas propiedades y valores:
        * `width: 50%;` Para que el contenido del div ocupe el 50% del width del padre
        * `display: inline-block;` De esta manera el div deja de intentar ocupar todo el ancho
        * `margin: 4%;` Le daremos un poco de espacio.
8. Vayamos a nuestra seccion `work-steps` y alejemoslo de los bordes.
    * `padding: 20px;`
    * `padding-bottom:40px`
9.  Dentro de la seccion anterior tenemos una lista ordenada con id y clase, cambiemos eso.
    * Crea en tu css, un selector a un id llamado `steps` **recuerda el box-sizing**
    * Colocale un width de 50vw;
    * alinea el texto a la izquierda
    * Borra la sombra del texto con `text-shadow:none`
    * Haz que la fuente sea negrita, con ` font-weight: bold;`
    * y centremos con un conocido amigo `margin: 0 auto;`
10. Ahora nos toca la seccion de values. Vamos a realizar algunos cambios.
    *  Dale un color de fondo `#545454cf;`
    *  Quitale todo el margen.
    *  Dale un `padding` de 20px;
    *  Demosle un color `#f0f0f0` para las letras.
11. A la lista dentro de la seccion values le daremos estilo:
    * Crea un id llamado `values-list` **recuerda el box-sizing** y dale las reglas
    *  `list-style: none;` para sacarle los bullets
    *  `line-height: 1.5;` Ya la habiamos configurado en una clase, pero solo se ocupaba una vez, mejor colocarlo aqui.
    *  `margin:0 auto;`
    *  `width:50vw;` Esta es otra forma, no hablamos del padre sino del 50% del viewport width
    * Luego agrega este id a la lista y quita la clase que tenia.

12. Vamos a crear un footer que acompañe a la pantalla:
    * Crea un selector de elemento footer
    * Dale un padding de: `15px;`
    * Dale un color de fondo `#2f2f2f;`
    * Un color:`#f9f9f9;`
    * Ahora si la propiedad **position**  que como queremos que este relacionada a la pantalla, utilizaremos fixed;
    * `bottom: 0px;`
    * `right:0px;`
    * `left:0px;`

13. Creamos el id `service-title` y le agregamos estas reglas:
    * `position: sticky;`
    * `top: 10px;`
    * color de fondo `#2f2f2f;`
    * `padding: 5px;`
    *  y un color `#f9f9f9;`
    *  Una sombra `0px -5px 10px 1px #545454`

## Conclusion
Bueno hemos visto muchas cosas que te permitiran modelar mejor tu sitio web, vamos a seguir trabajando en este proyecto. 
A este punto deberias tener [un sitio de este estilo][2]


[1]:https://tsc-clase-4.netlify.com/ "Sitio terminado"
[2]:https://tsc-clase-5.netlify.com/ "Sitio terminado"