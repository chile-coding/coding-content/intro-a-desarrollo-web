# user Stories
Fuimos contactados por el estudio de abogados que nos encargo su landing page.
Quieren sumar unas funcionalidades mas a la misma.
Sus requerimientos son:
* Un formulario de contacto
* Quieren saber el nombre, email y telefono del usuario.
* Quieren saber si en una empresa o un particular.
* El usuario debe de poder dejar un mensaje si lo desea.
* El usuario debe aceptar los terminos legales.
* El sitio debe ser amigable para el navegador
* Debe de tener una clara seccion del header
* Debe indicar cual es su contenido principal
* Debe indicar las distintas secciones del archivo.
* Debe tener un contenido de navegacion final

Seguiras trabajando con el archivo del ejercicio anterior.
[Esto es lo que haras][1]

## Creando un formulario
1. Ve a la seccion del **div con id="contact"**
    * Elimina todos los elementos `small`, (debe quedar solo el elemento `h3`)
    * **Debajo** del `h3` crea un elemento `form`
    * Arma un modelo de input `<label>xxx <br> <input type="" name="" required></label><br>`
    * Lo vamos a ocupar 4 veces.
2. Configuracion de los campos.
|texto de label | type | name | placeholder | required |
|--------------|:------:|:---:|:--------|:--------|
|Name| text | name |John Doe | true |
|Email| email | email |tsc@thesoftwarecamp.com | true |
|Tel| tel | tel |none| false |
|Legal Terms| checkbox |none| conditions | true |

3. Luego del campo "Tel" coloca un elemento `label` con el texto "Role", que contenga un elemento `select` con `name="role"` con estas opciones.
    * Una opcion de value="company" y texto "Company"
    * Una opcion de value="particular" y texto "Particular"
4. Una vez creado el campo anterior, a continuacion coloca un elemento `label` con el texto "Message", que contenga un elemento `textarea`.
    * Atributo name="message"
    * Atributos cols="30" y rows="10"
5. Crea un boton para enviar la informacion, podias hacerlo con los elementos `input` o `button` de type submit recuerdas?
6. Añade saltos de linea y texto en negrita para los Nombres de campo que indican los labels, de esta manera quedara mejor.

## Ser amigable para el navegador
1. Busca el `div` de `id="header"` y cambialo por un elemento `header`, dejando todo lo demas intacto (conservando el atributo id y envolviendo a los mismos elementos). 
2. Busca el `div` de `id="main"` y cambialo por un elemento `main` manteniendo todo como el punto anterior.
3. Envuelve el elemento `img`, que esta dentro de `header`, en un elemento `figure`.
4. Dentro del elemento `figure`, a continuacion del elemento `img` coloca un elemento `figcaption` con el texto "In God we trust".
5. Busca los div con los siguientes ids, y cambialos por un elemento `section` manteniendo los ids.
    * **service-list**
    * **work-steps**
    * **values**
    * **contact**
6. Ve al final del elemento `section` de id "contact" y coloca un elemento `footer` con el texto "Made with love by The Software Camp"
7. Listo tienes el challenge hecho!

[1]:https://tsc-clase-3-challenge.netlify.com/ "Landing page"