# Organizando nuestra landing Page
Bueno, la primer clase haz creado una landing page de un estudio de abogados recuerdas?
Bueno ahora lo que haras es seccionar con elementos div y span esa landing page de manera que sea mas facil para ti mantener el codigo y agregar funcionalidades luego (La proxima clase es de estilos, me quiero morir de la emocion)
## Trabajando sobre la landing
Te enseñaremos a ~~robar codigo~~ a influenciarte con codigo ya creado.
**Ten ya lista tu carpeta de la clase 3 con un archivo index.html creado**
1. Ve a [este sitio][1]
2. Una vez en el navegador, da un click derecho y seleccion la opcion "Ver codigo fuente de pagina"
    * Si estas buscando el modo programador para hacer esto es **Ctrl + U** (**Cmd + U** en mac)
3. Sobre esta pagina rara que abrio selecciona todo con **Ctrl + A** (**Cmd + A** en mac)
4. Luego **Ctrl + C** (**Cmd + C** en mac)
5. Ve al archivo `index.html` en tu carpeta 'clase-3' y pega el codigo con **Ctrl + V** (**Cmd + V** en mac)
6. Ahora guardalo **Ctrl + S** (**Cmd + S** en mac)
7. Listo nos hemos inspirado en codigo de otro! recuerda:
    > Un programador amateur copia codigo, un profesional lo roba directamente.
## Secciones generales
Ahora que ya tenemos andando el archivo de la landing page comencemos con unas reformas. **Comenta al cierre de cada div por su id.**`</div><!--Close header-->`
1. Ve al archivo y envuelve en un elemento `div` ,desde el elemento `h1` hasta el elemento `h2` sin incluirlo
    * Elementos envueltos : h1, p, nav, img 
    * Ademas coloca un atributo `id` de valor "header" en el `div`.
2. Luego envuelve en otro elemento `div`, desde el `h2` con id "about", hasta el `h3` con id "contact", sin incluir a este ultimo.
    * Ademas coloca un atributo `id` de valor "main" en el `div`.
3. Por ultimo envuelve la seccion de contacto, es decir el `h3` y los tres elementos `small`, en otro div.
    * Borra el atributo id="contact" del elemento `h3`
    * Coloca el atributo id="contact" en el div que acabas de crear.
## Secciones especificas
Hemos creado secciones generales de nuestro archivo, pasemos a crear algun mas especificas. Recuerda **comentar al cierre de cada div**.
1. Envuelve a toda la seccion de servicios en un **div** que tenga un atributo **id="service-list"**.
    * La seccion comienza en el elemento `h3` de texto "Service"
    * La seccion termina antes del elemento `h3` de texto "How we work"
2. Envuelve toda la seccion de etapas en un **div** que tenga un atributo **id="work-steps"**
    * La seccion comienza en el elemento `h3` de texto "How we work"
    * La seccion termina antes del elemento `h3` de texto "Why choose us"
3. Envuelve toda la seccion de valores en un **div** que tenga un atributo **id="values"**
    * La seccion comienza en el elemento `h3` de texto "Why choose us"
    * La seccion termina antes del elemento `div` de id="contact"
## Secciones repetidas
Vamos a trabajar dentro del **div con id "service-list"**
1. Identifica la parte que se repite. ¿Sabes cual es? exacto el combo de elementos `img`,`h4` y `p` de cada servicio
    * Envolve todo lo relacionado a el servicio "Comercial Law" en un `div` con `id="comercial-service"`
    * Envolve todo lo relacionado a el servicio "Supervision of Companies" en un `div` con `id="supervision-service"`

## Conclusion
Lo haz hecho bien. Trabajar con Span es muy parecido y lo veremos la clase siguiente.