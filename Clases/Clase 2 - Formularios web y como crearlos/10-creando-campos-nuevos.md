# Nuevos campos de formulario
Estos campos si bien no se hacen con los elementos inputs, no difieren de estos por mucho.
1. Coloca un elemento `label` nuevo en medio de los campos **Genre** y **Legal Terms** con el texto "Civil Status"
2. Dentro del `label` crea un elemento `select` y piensalo como una lista, que en vez de `li` tiene elementos `option`.
    * El atributo name va en el elemento select y no en sus elementos anidados, coloca su valor como "civil-status".
    * Crea un elemento `option` dentro con el atributo `value` de valor "single", de texto "Single"
    * Crea un elemento `option` dentro con el atributo `value` de valor "married", de texto "Married"
    * Crea un elemento `option` dentro con el atributo `value` de valor "no remember", de texto "No remember" y hazlo seleccionado por default.(Buscalo en google, tu puedes)
3. Coloca un elemento `label` luego del campo "Civil Status" con el texto "Message"
4. Dentro del `label` crea un elemento `textarea` y Coloca sus atributos:
    * `name` = "message"
    * `cols`="30"
    * `rows`="10"
    * `placeholder`="Hi i am a new developer searching for friends..."
    * No lo hagas obligatorio, (Quitale el required si lo tiene)

## Final
Has llegado al final de la clase si quieres puedes ponerlo mas bonito al formulario:
* Colocale un titulo
* Los textos de los elementos label en negrita quedarian mejor.
* Un poco de espacio le vendria bien entre el nombre del campo y el campo mismo.

Aqui debe estar [un sitio parecido al tuyo][1]

[1]:https://tsc-clase-2.netlify.com/ "Ejemplo de formulario"