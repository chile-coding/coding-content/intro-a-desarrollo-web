# Creando un formulario

## Armando el ambiente
El primer paso como siempre es tener todo ordenado si? Recuerdan la carpeta "curso-tsc" de la clase anterior?
1. Crear la carpeta "clase-2" dentro de la carpeta "curso-tsc"
2. Crear un nuevo archivo HTML llamado "index" como lo hace un programador.
    * **Ctrl + N**, (**Cmd + N** en mac)
    * **Ctrl + S**, (**Cmd + S** en mac)
    * No olvides agregarle el formato ".html" antes de guardarlo.
3. Escribe los elementos basicos para un archivo html y guardalo (Puedes usar el atajo o practicar escribir Doctype, html, head y body tu mismo)

## Creando el form
1. Crear un elemento form, recuerda que utiliza los open y closing tags.`<form></form>`
2. Anidado, es decir, dentro del elemento form crea un elemento input, recuerda que es un self closing tag.`<input>`
3. Los elementos input tienen un atributo que los define, este es el atributo `type`, agregalo con valor "text"
    * Recuerda que los atributos se escriben "nombre de atributo" + "signo igual" + "valor entre comillas" y siempre en el opening tag, ya que definen como debera crear el navegador el contenido incluido en el elemento (Si lo pusieramos al final, el contenido ya estaria creado)
    * En este punto deberias tener algo asi:
    ```HTML
    <form>
        <input type="text">
    </form>
    ```
4. Otro atributo muy utilizado es el `placeholder` nos permite indicar con un ejemplo, el tipo y formato de la informacion que el usuario deberia ingresar.
    * Coloca un atributo placeholder a tu input con el valor "John Doe" asi sabra que debe colocar su nombre.
5. Agrega un atributo `id` con valor "name" para saber referenciarlo. 
6. Para hacer nuestro formulario mas accesible por el navegador, podemos incluir un Label.
    * Coloca un elemento label antes de tu elemento input.
    * Dale al label un atributo `for` para referenciar al input al que representa, y de valor coloca "name" (Debe tener el mismo valor que el id del input)
    * Agrega un texto para el Label, lo mas conveniente en este caso es la palabra "Name".
7. Muy bien! haz creado el primer formulario. Deberia verse asi:

```HTML
    <form>
        <label for="name">Name</label>
        <input type="text" placeholder="John Doe" id="name">
    </form>
```