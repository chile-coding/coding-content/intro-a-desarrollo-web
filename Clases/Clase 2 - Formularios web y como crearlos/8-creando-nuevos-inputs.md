# Creando nuevos inputs
## Campos obligatorios
1. Ve al input type text y coloca la palabra `required` simplemente.
2. Ahora intenta enviar el formulario sin completar ese campo.
## Creando inputs
Vamos a crear muchos inputs asi que lo haremos a un modo programador.
1. Copia tu input type text y pegalo debajo de si mismo.
2. Deja todos los atributos menos el id y coloca sus valores con "" para hacerlos vacios.
`<input type="" placeholder="" name="" required>`
3. Envolve este input en un elemento `label` (Otra manera de utilizarlo) y dale un texto base "xxx" que despues cambiaremos.
3. Bien! tenemos lo que se dice un template, o modelo.`<label>xxx<input type="" placeholder="" name="" required></label>`
    * Ahora vamos copiarlo
    * Replicarlo 3 veces mas para asi quedar con 4 nuevos elementos input a completar.
4. Al primer input llenalo de la siguiente manera para tener un campo de contraseña:
    * De texto de `label` coloca "Password"
    * `type` = "password"
    * `placeholder` = "*******"
    * `name` = "password"
    * `required`
4. Al que sigue lo utilizaremos para obtener el email del usuario:
    * De texto de `label` coloca "Email"
    * `type` = "email"
    * `placeholder` = "tsc@thesoftwarecamp.com"
    * `name` = "email"
    * `required`
4. Al siguiente input **duplicalo** con esta configuracion:
    * De texto de `label` coloca "Genre" (Un solo `label` debe abrazar a ambos inputs de tipo radio)
    * `type` = "radio"
    * `name` = "genre"
5. Luego agregale valores:
    * Al primer radio`value`="male" y texto "Male"
    * Al segundo radio`value`="female" y texto "Female"

6. Al ultimo input vacio llenalo de la siguiente manera para tener un check.
    * De texto de `label` coloca "Legal Terms"
    * `type` = "checkbox"
    * `name` = "legal-terms"
    * `value`= "ok"
    * `required`
    * Con el texto "I accept all terms"

7. Volvemos a los inputs radio, y pongamos a female, como default. 
    * Busca en google como poner un elemento como [checkeado por default][1]
8. Nuestro formulario se ve un poco feo. Pongamos saltos de linea entre cada campo
    * Si no recuerdas con que elemento se hace puedes [buscarlo en google][2]

## Conclusion
Tu formulario deberia verse un poco asi:
``` HTML
    <form>
            <label for="name">Name</label>
            <input type="text" placeholder="John Doe" id="name" name="name" required>
            <label>Password
                <input type="password" placeholder="******" name="password" required>
            </label>
            <label>Email
                <input type="email" placeholder="tsc@thesoftwarecamp.com" name="email" required>
            </label>
            <label>Genre
            <input type="radio" name="genre" value="male" required>Male
            <input type="radio" name="genre" value="female" required checked>Female
            </label>
            <label>Legal Terms
                <input type="checkbox" name="legal-terms" value="ok" required> I accept all terms
            </label>
            <button type="submit">TSC Rules!</button>
    </form>
```
[1]:https://www.google.com.ar/search?q=hacer+un+elemento+radio+checked+por+default "Busqueda en google"
[2]:https://www.google.com.ar/search?client=ubuntu&hs=elY&sxsrf=ACYBGNRHyMzQDjWEVVFsapuVajestSVMRg%3A1568319618264&ei=gqh6XYzOD7e85OUPyIq-6AE&q=elemento+html+de+salto+de+linea&oq=elemento+html+de+salto+de+linea "Elemento de salto de linea"