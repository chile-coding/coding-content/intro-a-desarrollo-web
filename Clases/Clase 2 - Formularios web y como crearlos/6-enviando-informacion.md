# Enviando informacion
## El boton de enviar
Ahora que tenemos el formulario creado deberiamos ser capaces de completarlo y enviarlo
1. Agrega un boton de enviar:
    * Crea un nuevo elemento input debajo del ya existente
    * Cual es el primer atributo que se debe pensar para un input? El `type` acertaste! dale un type con valor "submit".
## Enviando info
1. Completa el input y dale click a send!
    * ¿Que paso? ¿ves un cambio?
2. Nos esta faltando nombrar a los campos de nuestro formulario!
    * Ve al input type text y agregale un atributo `name` y ponle de valor `name` o como quieras que se llame el campo que tendra esta informacion en tu base de datos.    
3. Graba el archivo y refrescalo desde el navegador. Vuelve a rellenar todo y dale send!

## Submit
Podemos utilizar un elemento button ademas para enviarlo,  de esta manera controlariamos mejor el texto del boton no te parece? 
1. Borra el input de type submit, y en su lugar coloca un elemento button con un atributo type de valor submit a su vez y como texto la palabra que desees.

Deberias tener un formulario de este estilo 
```HTML
    <form>
        <label for="name">Name</label>
        <input type="text" placeholder="John Doe" id="name" name="name">
        <button type="submit">TSC Rules!</button>
    </form>
```