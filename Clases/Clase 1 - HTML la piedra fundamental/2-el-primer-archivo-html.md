# Si! vamos a crear nuestro primer archivo!

## Aclarando dudas
Nuestro **archivo html** debe estar ubicado **en una carpeta especifica** en nuestro ordenador y es una buena practica **nombrar todo con minusculas**.
¿Por que? Muy buena pregunta!
1. Nuestros **proyectos** en el futuro van a ser **un grupo de archivos** que estan relacionados entre si, no uno solo.
1. En programacion se acostumbra tener **estandares en creacion de codigo** y nombre de archivos. ej `index.html`
2. Si creamos carpetas especificas para cada proyecto podremos seguir estas "Buenas practicas" **sin tener conflicto** con archivos creados anteriormente.
## Creando mis carpetas
Bueno una vez aclaradas las dudas vamos a **crear nuestra carpeta**.
* Dirigite al escritorio (La pantalla principal) de tu ordenador y crea una nueva carpeta llamada "curso-tsc" e ingresa a la misma.
* Dentro de la carpeta "curso-tsc" crea otra carpeta que se llame "clase-1".

## Utilizando el editor de texto
El editor de texto es un programa que nos permite crear nuestro codigo, que al fin y al cabo son letras, numeros y simbolos, osea texto.
Pensalo como un word pero optimizado para programar.

Puedes crear un nuevo tradicionalmente o como lo hace un programador, nosotros practicaremos ambas.

### Creando un archivo tradicionalmente
* Abrí el visual studio code ( o cualquier editor que utilices)
* Ve a la esquina superior izquierda y dale click a "File"
* Luego haz un click en "New File"
* Veras un archivo nuevo vacio llamado generalmente "Untitled -1"
* Pega este codigo dentro del archivo
```HTML
<!DOCTYPE html>
<html>
        <head>
            <title> Mi primer archivo </title>
        </head>
        <body>
            <h1>The software camp rules!</h1>
        </body>
</html>

```
* Las letras se ven grises no? este archivo aun no tiene un formato para nuestro ordenador y debe ser guardado en nuestra carpeta "clase-1".
* Dirigete a File nuevamente y dale click a "Save as" (Guardar como) navega hasta tu carpeta "curso-tsc", ingresa luego a "clase-1". 

    Para guardar tu archivo tienes que indicar el nombre y su formato. 

    > El formato sirve para que nuestro ordenador sepa que software utilizar para entender a nuestro archivo. Generalmente se lo indicamos con un punto y el nombre del formato al final.

    coloca "index" como nombre de archivo y ".html" como formato quedando `index.html`
* Minimiza tu editor de texto, ve al escritorio y a tu carpeta "curso-tsc/clase-1", dale doble click a "index" 
* **Si! hicimos nuestro primer archivo**

### Eliminando nuestro archivo
Esto es doloroso, lo sabemos, pero es necesario y se puede hacer de dos maneras.
* Eliminas "index" desde la carpeta "clase-1" como has eliminado cualquier documento, foto o archivo en tu vida.
* Desde el editor de texto
    * Vas a "File"
    * luego a "Open Folder"
    * Click derecho sobre tu archivo y eliges "Eliminar"

### Creando un archivo modo programador
Esto es como lo haremos de aqui en mas.

* Abrí el visual studio code ( o cualquier editor que utilices)
* tipea **Ctrl + N** (**Cmd + N** en Mac)
* Veras que se creo tu archivo "Untitled -1"
* Tipea **Ctrl + S** (**Cmd + S** en Mac), luego navega hasta tu carpeta "curso-tsc", ingresa luego a "clase-1". 
* Guardalo como lo hicimos en el proceso tradicional con el nombre `index.html`
* Posicionate en el archivo y escribe "html" y veras un menu
    * Elige el 2do con las flechas del teclado y dale enter.
    * Tambien puedes darle click con el mouse.
* Genial! Somos magos y hemos sacado codigo de la galera.
* Hicimos un cambio, debemos volver a grabar el archivo con **Ctrl + S** (**Cmd + S** en Mac), ya lo hara sobre la carpeta "clase-1" automaticamente.
* Minimiza tu editor de texto, ve al escritorio y a tu carpeta "curso-tsc/clase-1", dale doble click a "index" 
* **Si! hicimos nuestro primer archivo como todo un programador**

## Competencia
Elimina index.html y vuelve a crearlo tomandote el tiempo. Puedes hacerlo mas rapido!
**Tenlo a mano por que trabajar en este archivo al continuar la clase.**

## Conclusion
 ¿Que acabamos de hacer aqui? Aprendimos que:
 * Programar tiene similitudes con nuestra manera tradicional de ocupar la PC
 * Hay beneficios de utilizar un editor de texto, ya que tiene herramientas especificas para la programacion como codigo basico etc...
 * Un buen programador minimiza el tiempo de las tareas ocupando "atajos" o "shortcuts" de teclado lo mas posible.
 
 **Recomendacion** La clases siguientes haran estos pasos en segundos, practiquen en sus casas utilizar los atajos, les traera mucha felicidad!



