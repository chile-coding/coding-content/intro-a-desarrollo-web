# Escribiendo buen HTML

Vamos a trabajar sobre el archivo que han creado en el ejercicio anterior. Pero descuida, puedes consultar al profesor.

## Alimentando al body
Mira cuidadosamente en tu archivo el elemento `body` no tiene nada entre su opening y closing tag.
1. Incluye dentro de body un elemento `h1` con el texto "Aprendiendo a programar"

2. Dentro de body, y **despues** (no dentro) del `h1` crea un elemento `p` con una descripcion corta con este formato "Toda nueva experiencia como viajar, un nuevo empleo, aprender a programar, puede ser inquietante, y los beneficios que se obtienen valen la pena. Soy .....(coloca tu nombre) y busco aprender a programar por que .....(una razon)"

* Luego del elemento `p`, debes crear un elemento `h2` con el texto "Que aprendere"

Abre tu archivo desde el navegador, estas creando un sitio web ya mismo!

## Comentarios
Los comentarios son avisos o mensajes que como desarrolladores de software dejamos para que la persona que le toque en el futuro "mantener" o "restaurar" nuestro codigo, no pierda tiempo intentando entendernos.
Tambien los utilizamos para dejar codigo escrito pero que el navegador no lo muestre, es muy comun cuando buscas ver como afectan los cambios a tu archivo html.
* Crea un comentario en cualquier lugar de tu html, a modo de "Opening tag" se utiliza `<!--` y como "closing tag" `-->`, solo debes colocar esto. 
```html
<!--
Aqui dentro puedes colocar lo que desees y no sera utilizado por el navegador como codigo, pero si quedara en tu archivo. Mas adelante lo veremos con el inspector de sitios web.
 -->
```
* Genial, ahora para probarlo de una manera practica, crea otro elemento `p` a continuacion del `h2` y comentalo. 
> Debes envolverlo entre `<!--` y `-->`. 
* Abre tu archivo en el navegador, deberia parecer que nada ha cambiado.
* Elimina el ultimo elemento `p` creado junto con los comentarios.

## Conclusion
Nos estamos moviendo como un pez en el agua con esto! tenemos un archivo y tres elementos nuevos dentro del body!