# user Stories
Fuimos contactados por un estudio de abogados que nos encargo su landing page.
Sus requerimientos son:
* Las personas al ingresar deben ver un titulo con el nombre del estudio
* Se debe ver una descripcion del mismo
* Una foto de perfil se encontrata justo debajo de la descripcion
* La biografia debe tener una seccion con mas detalles
* Se deben incluir los servicios que dan
* Se debe incluir cuales son las etapas de contratacion
* Las personas deben tener un apartado de por que contratarlos
* Una seccion de contacto es necesaria
* El sitio dispone de una botones de navegacion
* Los enlaces de navegacion indican a las secciones de Home, About us y Contact.

[Lo que nos piden][1]

## Realizando el sitio
Si terminaste la clase, deberias tener un [sitio igual a este][2] y al mirar los requerimientos del estudio de abogados, se daran cuenta que el sitio es muy parecido a todo lo que veniamos trabajando, no habra **nada nuevo**, solo **practica que es muy importante!**

Crea un archivo html nuevo desde cero, coloca manualmente sus 4 tags principales `DOCTYPE`, `html` , `head`, y `body`
1. **Dentro** del elemento `head` (entre su open y closing tag) crea un tag title con el nombre "Honesty & Heart", quedando:

 ```HTML
    <head>
        <title>Honesty & Heart</title>
    </head>
 ```
2. **Dentro** del elemento `body` (entre su open y closing tag) crea un elemento `h1` con el texto "Honesty & Heart" y colocale el atributo `id` con el nombre "title" `<h1 id="title">Honesty & Heart</h1>` 
3. Dentro de `body` a continuacion del `h1` coloca un elemento `p` con el texto "We are a very competent, sincere and big-hearted law firm." y añadele un atributo `id` de valor "description" `id="description"`.
Tu archivo en este punto deberia verse asi:
 ```HTML
    <head>
        <title>Honesty & Heart</title>
    </head>
    <body>
        <h1 id="title" >...</h1>
        <p id="description" >...</p>
    </body>
 ```
 Para lo que sigue ten en cuenta que quiere decir *dentro del tag* o *un atributo ... de valor ... * y recuerda que todo lo has visto en clase.

4. A continuacion del elemento p que creaste en el punto anterior, coloca un elemento `img` con atributos:
    * src con la direccion de la imagen "https://www.sierraclub.org/sites/www.sierraclub.org/files/styles/flexslider_full/public/sierra/articles/big/SIERRA%20Cool%20Schools%20Environmental%20Lawyer%20cover%20WB.jpg?itok=NS5NfEP_"
    * alt que de valor tenga "Honesty & Heart values"
5. Luego agrega un elemento `h2` con id "about" que diga "About Honesty & Heart"
6. Luego agrega un elemento `h3` con el texto "Service"
7. Agrega a continuacion un elemento `p` con este texto:
    >Our law firm has decades of experience in mainly commercial matters. The practice areas include general commercial law, distribution and agency law, establishment and supervision of companies, debt collection at a national and an international level, maritime and transport, vessel arrest, intellectual property and counterfeit, among others.
8. Agrega una imagen con el atributo src ="https://hbfiles.blob.core.windows.net/images/homepageimage/88328_image.png" y alt="commercial law service"
9. Luego agrega un elemento `h4` con el texto "Commercial Law"
10. A continuacion coloca un parrafo para descripcion de este servicio con "lorem..." [que es lorem ipsum][3] y como [utilizarlo en visual studio code][4].
11. Agrega una imagen con el atributo `src` de valor "https://susangamache.com/wp-content/uploads/2017/10/mature-businessman-addressing-boardroom-meeting-P22KTFZ.jpg" y el atributo `alt` de valor "supervision of companies"
12. Luego agrega un elemento `h4` con el texto "Supervision of companies"
13. Repetir punto 10

14. Luego agrega un elemento `h3` con el texto "How we work"
15. A continuacion coloca una lista ordenada (elemento `ol`) que contenga como items (`<li>`) lo siguiente:
    * You contact us
    * We meet you and discuss the project
    * We start to doing it
16. En el primer item, con texto "You contact us" debemos anidar una lista desordenada(`ul`) con estos items:
    * By email
    * By whatsapp
    * By form submition
17. Luego agrega un elemento `h3` con el texto "why choose us"
18. A continuacion coloca una lista desordenada que contenga como items:
    * We have this huge value
    * We also have a lot of experience
    * We have the best price
19. Luego agrega un elemento `h3` con el texto "Contact us" y con `id` de valor "contact"
20. Coloca un elemento `small` para cada dato y resalta el concepto con un elemento `b` para los siguientes:
    * Tel:11111111111
    * Email:codin@codin.com
    * Direction:Av always happy 123
    **Ejemplo 1 -**   `<small><b>Tel:</b>111111</small>`

    > Recuerda que el elemento b coloca el texto en negrita, pero solo por estetica, el elemento strong le indica al navegador que ademas esas palabras son importantes.

    * El elemento small es inline por lo que debes darle un `br` al final para que saltee la linea en cada uno.

21. Volvamos al principio del body, entre el elemento `p` con id "description" y la imagen, coloca un elemento `nav` que **dentro**(anidado) contiene tres botones con un enlace cada uno (elementos `a`) :
    *  el primero tiene de atributo href el valor "#" y el texto "Home"
    *  el segundo tiene de atributo href el valor "#about" y el texto "About us"
    *  el tercero tiene de atributo href el valor "#contact" y el texto "Contact"
    **Ejemplo 1 -**   `<button><a href="#">Home</a></button>`

22. Vamos a colocar tu sitio en internet
    1. Dale un click [**aqui**][5]
    2. Veras un recuadro que dice "Drag and drop your site folder here"
    3. Arrastra la carpeta que tiene este archivo para tener **tu primer sitio online!**
    * Debes arrastrar **la carpeta** no el archivo.
    * El nombre del archivo debe ser **index.html**
23. Muestrale a todos tus familiares y amigos de lo que fuiste capaz en solo una clase!

[1]:https://tsc-clase-1-challenge.netlify.com/ "Landing Page"
[2]:https://tsc-clase-1.netlify.com/ "Ejercicio en clase"
[3]:https://neoattack.com/neowiki/lorem-ipsum/#que-es "Explicacion de lorem ipsum"
[4]:https://www.youtube.com/watch?v=LZIS07X-FuI "Lorem en visual studio code"
[5]:https://app.netlify.com/drop "Netlify drop"