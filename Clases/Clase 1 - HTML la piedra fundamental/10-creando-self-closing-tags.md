# Self closing tags
Son elementos que no tienen una etiqueta de cierre y contienen funcionalidades muy particulares

1. Ve a la seccion de habilidades tecnicas que trabajamos en el punto anterior, y al final de la misma,(cuando cierra la lista ordenada) coloca un elemento `<hr>` que nos divida de la seccion que sigue.
2. Ve al elemento `p` que esta a continuacion del elemento `h3` con el texto "Que aprendere".
    * Cambialo su texto a 
        >"El curso de introduccion a la programacion nos enseña de manera practica: *
        El mejor camino para comprender y proyectarse dentro del desarrollo de software *
        Conocimiento real del mercado y la dinamica profesional que un programador debe llevar"

    * Si lo abres desde el navegador, seguira viendose el texto sin saltos de linea, es por esto que debemos colocar un elemento `br` en cada asterisco (*) que veamos.
3. Guarden y abran su archivo en el navegador para ver la magia de los elementos con self closing tags