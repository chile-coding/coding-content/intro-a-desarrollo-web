# Crear elementos con atributos
## el atributo id
1. Ve al elemento h3 con el texto "habilidades tecnicas" y colocale un atributo id con valor "tech-skills" `<h3 id="tech-skills">...</h3>`
1. Ve al elemento h3 con el texto "habilidades profesionales" y colocale un atributo id con valor "soft-skills"
## Enlaces en palabras
Asi como utilizamos elementos `strong` y `em` tenemos que utilizar elementos `a` para crear enlaces en frases o palabras.
1. Ve al parrafo de la seccion "Que aprendere" 
    * Envolve la frase "comprender y proyectarse" en un elemento `a` con el atributo `href` con valor "#tech-skills" `<a href="#tech-skills">comprender y proyectarse</a>`
    * Envolve la frase "dinamica profesional" en un elemento `a` con el atributo `href` con valor "#soft-skills".
## Creando imagenes con enlaces
1. Ve al parrafo de la seccion "Aprendiendo a programar" y a continuacion del mismo crear un elemento `img` que tenga como atributo `src` el valor "url al logo de TSC" y como atributo `alt` el valor "The Software Camp Logo"
2. Envolver la imagen creada en un elemento `a` con atributo `href` de valor "https://www.thesoftwarecamp.com" 
Quedando:
```HTML
...</p>

    <a href="https://www.thesoftwarecamp.com">
        <img src="" alt="The Software Camp Logo">
    </a>

    <h2>Que aprendere ...
```
3. Guarda tu archivo y abrilo desde el navegador para comprobar que esos enlaces nos dirigen a elementos con esos id y paginas.
