# Creando elementos con display inline
Ya conocemos algunos elementos que su propiedad display tiene valor block, haciendolos individualistas ocupando todo el ancho de la pantalla sin compartir con los demas.
Tambien existen elementos tolerantes que solo se enfocan en lo suyo y no alteran la ubicacion del resto.
## Agregando elementos a nuestro archivo
* Ve al elemento `h1` en nuestro archivo y envuelve su texto en elemento `u` para subrayarlo quedando `<h1><u>..texto..</u></h1>`
* Ve al texto de nuestro elemento `p` y envuelve solo la palabra "inquietante" en el tag opening y closing tag del elemento `s` para tachar texto.
* Sobre nuestro elemento `p` aun, encuentra la frase "nueva experiencia" y la palabra "beneficios" y envuelvelas con el elemento `strong`.
* Usa el elemento `em` para poner en cursiva el texto "viajar, un nuevo empleo, aprender a programar" en el elemento `p`.
## Conclusion
Podemos gracias a estos elementos agregar formatos a secciones especificas de nuestro contenido, sin alterar su ubicacion o presentacion en el archivo. Como ven, estos formatos son predefinidos y limitados, en el futuro utilizaremos "elementos de seccion" que nos permitiran seleccionar contenido especifico y darle todo el estilo que queramos!
Paciencia aun nos queda mucho por aprender.