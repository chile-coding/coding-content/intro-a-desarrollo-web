# Anidando elementos
Ya hemos trabajado con anidacion sin saber el concepto, cada vez que colocamos un elemento strong o em dentro de nuestro elemento p, estabamos efectivamente anidandolo. Pero las anidaciones tienen un formato a su vez en ciertos casos y es con las listas en donde se percibe aun mas.

## Creando nuevas secciones
Repasemos nuestro archivo: 
* Titulo con elemento `h1`
* Parrafo con el elemento `p`
* Subtitulo con el elemento `h2`

Ahora debemos crear en la seccion "Que aprendere" un apartado para habilidades tecnicas, y habilidades profesionales.

1. A continuacion del `h2` coloca un parrafo con el elemento `p` que contenga el siguiente texto 
> "El curso de introduccion a la programacion no solo nos enseña de manera practica el mejor camino para comprender y proyectarse dentro del desarrollo de software, sino que ademas nos empapa con un conocimiento real del mercado y la dinamica profesional que un programador debe llevar"
2. Luego del elemento `p` coloca un elemento `h3`
3. Copia el h3 y replicalo debajo. Hay 2 maneras de hacer esto
    * Tradicional y lenta
        * Marcar con el mouse el h3 seleccionandolo
        * Darle un click derecho para abrir el menu
        * Seleccionar copiar
        * Dar click derecho debajo de ellos y seleccionar pegar.
    * Modo Programador
        * colocar el cursos en el comienzo del elemento h3
        * Mantener apretado **Shift** 
        * Envolver al h3 con las flechas del teclado
        * Una vez ahi, soltar **Shift**
        * Tipear **Ctrl + C** , para copiar la seleccion
        * Luego **Ctrl + V** para pegar la seleccion.

Utilizar los atajos del teclado seleccionando con `shift`, y utilizando funciones de `ctrl`, les hara la vida mas facil.

4. Ahora que tenemos:
```html
<!--Codigo anterior-->
....</p>
<h3></h3> 
<h3></h3> 
```
* Coloca como texto en el primero "Habilidades Tecnicas"
* Coloca como texto del 2do "Habilidades profesionales"

## Creando listas
1. A continuacion del elemento `h3` de habilidades tecnicas crea una lista ordenada que tenga como items "HTML", "CSS", "JS", "Frameworks"
```HTML
<ol>
    <li>HTML</li>
    <li>CSS</li>
    <li>JS</li>
    <li>Frameworks</li>
</ol>
```
2. A continuacion del elemento `h3` de "habilidades profesionales" crea una lista ordenada que tenga como items "Buenas practicas", "Trabajo en equipo", "Pensamiento paralelo". Recuerda que es muy parecido al punto anterior solo se debe cambiar `ol` por `ul`, y ambos ocupan elementos`li`

## Creando una lista anidada
Solo los elementos li tendran el formato de viñetas o numeracion. Vamos a comprobarlo.
1. En el ultimo elemento de la lista ordenada, el que dice "Frameworks":
    * Presiona enter al final de la palabra Frameworks
    * Dentro del mismo elemento `li`, coloca una lista desordenada con sus propios elementos `li` "Bootstrap" y "jQuery":
```
    <li>Frameworks
        <ul>
            <li>Bootstrap</li>
            <li>jQuery</li>
        </ul>
    </li>

```
## Conclusion
Guarda tu archivo y abrelo desde el navegador para lo que haz creado.