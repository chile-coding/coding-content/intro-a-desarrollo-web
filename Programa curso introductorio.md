#Programa del curso
Los objetivos son:
1. Tener el claro el alcance del curso de introduccion a la programacion.
2. Dividir el contenido en clases
3. Especificar en cada clase que va a ser dictado por el profesor y que conocimientos se tendran con el challenge
4. Crear el contenido
5. Crear los challenge

# clases
## Clase 1 - HTML la piedra fundamental.
### Fundamentos de HTML
* Conocer los comienzos de internet **Solo en video**
* Como funciona Internet, DNS, IP **Solo en video**
* Que es HTML 
* Como se escribe HTML
* el flag `DOCTYPE`
* Que es un elemento o Tag.
* Opening and Closing tag.
* los elementos `<html>`, `<head>` y `<body>`.
* Estructura de un documento.

### El primer archivo HTML (practico)
* Crear un documento html basico.
* Eliminar el documento creado.

### Elementos principales
* Conociendo los headers `<h1>` y sus amigos.
* Creando parrafos con el elemento `<p>`

### Escribiendo buen HTML(practico)
* Incluyendo un titulo
* Incluyendo un parrafo
* Incluyendo un Subtitulo
* Comentando el body
* Comentar solo un parrafo
* Eliminar un elemento html

### Que es el display
* Display: block que es?
* Display: inline que es?

### Creando elementos con distintos display (practico)
* Use el tag `<strong>` o el `<b>` para hacer que el texto sea negrita
* Usa el tag `<u>` para subrayar el texto
* Usa el tag `<em>` para poner en cursiva el texto
* Usa el tag `<s>` para tachar texto

### Anidando elementos
* Que son los elementos de listas `<ul>`, `<ol>`? 
* Que es un `<li>`
* Cuales son sus diferencias.

### Creando anidaciones (Practico)
* Usa el tag `<ul>` para crear una lista desordenada
* Usa el tag `<li>` para crear un elemento de lista
* Dentro del tag`<ul>`, usar el tag `<ol>` para crear una lista ordenada
### Self closing tags
* Explicacion
* Ejemplos de elementos self closing tags
* `<hr>`,`<br>`
### Creando self closing tags (Practico)
* Crear una línea horizontal con el elemento `<hr>`
* Crear un salto de linea con el elemento `<br>`
### Que son los **atributos html**
* Sintaxis de un atributo
* atributo `id`
### Elementos con atributos fundamentales
* el elemento `<a>` para crear enlaces.
* atributo `href`.
* El elemento `<img>`
* Que es el atributo `alt`.**Solo en video**
* Cuándo se debe dejar en blanco el atributo `alt`.**Solo en video**
* Atributo `src`
### Creando elementos con atributos (Practico)
* Parrafos con enlaces
* Crear `<img>` como enlaces
* enlace muerto con el `href="#"`
* enlaces Internos al `id`
* `href` con enlaces externos.
### Challenge Clase 1


## Clase 2 - Formularios web y como crearlos
### Formularios en la web
* Ejemplos de formularios
### El elemento form
* Para que sirve el elemento `<form>`
* ¿Que tipo de display tiene?
### Campos de un formulario
* los inputs
* los inputs de tipo texto
* que es un placeholder?
* que son los labels y la web semantica?
### Creando nuestro primer formulario (Practico)
* Crear un input de type text
* Agregarle un placeholder
* Agregarle un label
### El viaje de la informacion
* El input submit.
* Todo campo debe tener un name.
* La informacion en nuestra url.
* Como viajaba la informacion antes? **Solo en video**
* Seguridad con Base64? **Solo en video**
* Que es http y https? **Solo en video**
### Enviando informacion (Practico)
* agregar un input submit
* agregar un atributo name
* cambiar el input submit por un boton.
* Enviar info
### Conociendo mas sobre inputs
* Enviar con required
* Type password y un truco de magia.
* Type email
* Type radio y sus usos
* Type Checkbox
### Creando nuevos inputs (practico)
* Colocar required
* Crear un input type password
* Crear un input type email
* Crear un input type radio
* Crear un input type checkbox
* Ponerlos el radio y el checkbox por default.
### Campos que no son inputs
* Opciones con `<select>` y `<option>`
* Mensajes usando `<textarea>`
* Donde se coloca el atributo name en estos casos?
### Creando campos nuevos (Practico)
* Utilizar `<select>`
* Utilizar `<textarea>`
### Enviando informacion a nuestro email
* Los atributos `action` y `method`
* Utilizar formspark
* Deploy en netlify
* Testear el email

** Juntamos el challenge de esta clase con la clase 3 **

## Clase 3 - HTML avanzado 
### Elementos de seccion
* Que es un div
* Que es un span
### Organizando nuestra landing page (Practico)
* Trabajar sobre el archivo
* Organizar secciones con div
* Organizar secciones con span
### HTML y la accesibilidad.
* Qué es SEO? **Solo en video**
* Como funcionan los motores de busqueda de google. **Solo en video**
* El elemento `<header>`
* El elemento `<main>`
* Los elementos `<figure>` y `<figcaption>` 
* Conociendo el elemento `<section>` ya lo teniamos.
* Conociendo el elemento `<footer>`
### Challenge de la clase 3 (Practico)
* Crear un formulario de contacto
* sumar elementos de accesibilidad

## Clase 4 - CSS, el estilo de nuestro sitio.
### Fundamentos de CSS
* Que es CSS
* Como se escribe CSS
### Nuestro primer estilo
* La propiedad `color`
* El selector de elemento.
### Las tres maneras dar estilo
Incluir la teoria, tienen un practico sobre el archivo a cargo de fsbalbuena, lo agrega luego.
* CSS con el **atributo style**, y el mantenimiento de codigo.
* CSS con el **tag style** y la duplicacion de contenido.
* CSS con un **archivo externo**, y sus beneficios.
### Clases, la magia en css.
* El atributo `class`
* El selector de id
* El selector de clase
* Sumando estilos, la diferencia entre id y clases.
### Dando estilo a nuestro proyecto (Practico)
* Usar el **atributo id** para dar estilo a un elemento
* Usa una **clase CSS** para dar estilo a un elemento
* Usa una clase CSS para dar estilo a mas de un elemento
* Coloca mas de una clase a un elemento.
### Propiedades para texto
Incluir la teoria, tienen un practico sobre el archivo a cargo de fsbalbuena, lo agrega luego.
* La propiedad `text-align`
* La propiedad `font-size`
* La propiedad `text-transform`
* La propiedad `font-weight`
* La propiedad `line-heigth`
### Propiedades avanzadas
Incluir la teoria, tienen un practico sobre el archivo a cargo de fsbalbuena, lo agrega luego.
* La propiedad `width` y `height`
* La propiedad `box-shadow`
* La propiedad `opacity`
* La propiedad `border` y `border-radius`
### Conceptos claves de CSS
Incluir la teoria, tienen un practico sobre el archivo a cargo de fsbalbuena, lo agrega luego.
* Herencia
* Especificidad
* Usar Important!
* Re escribir estilos con una declaracion css
* Re escribir estilos con otro archivo css

** Esta clase no tiene challenge ya que se trabaja sobre el proyecto de landing en todos los practicos **

## Clase 5 - Conceptos de CSS
### Teoria de colores
* Los colores en Hexadecimal y RGB.
* Uso de código hexadecimal abreviado
### Modelo de cajas
* La propiedad `border`
* La propiedad `padding`
* La propiedad `margin`
* Usar notación en sentido horario para especificar el `margin` o el `padding` de un elemento.
* La propiedad box-sizing.
### Posicionamiento
* la propiedad `position`
* el valor `relative` de la propiedad position
* el valor `absolute` de la propiedad 
* el valor `fixed` de la propiedad position
* el valor `static` de la propiedad position
* la propiedad `float`
* Que es la propiedad `z-index`
### challenge
* Aplicar modelo de cajas
* Crear barras con posicionamiento
## Clase 6 - Selectores CSS nivel avanzado
### Selectores avanzados
* Selector universal
* Selecciones simultaneas
* Selector descendiente
* Selector hijos
* Selector adyacente
### Refactor de nuestro proyecto (Practico)
* utilizar Selector universal para el box-sizing y herencias
* utilizar Selecciones simultaneas para unificar estilos
* utilizar Selector descendiente 
* utilizar Selector hijos
* utilizar Selector adyacente
### Selectores avanzados especificos
* Selectores de atributos
* Selector por indice
* Pseudo elemento ::placeholder
* Anidando selecciones **Solo en video**
### Segundo refactor a nuestro proyecto (Practico)
* Utilizar selector de atributos en inputs
* Utilizar Selector por indice en elementos headers
* Cambiar el color del placeholder de los input de tipo texto.
### PseudoSelectores
* Utilizar `:hover`
* Utilizar `:active`
* Utilizar `:focus`
* Utilizar `:required`
### Cambiando la fuente
* Importar una fuente de Google
* Establecer la familia de fuentes de un elemento
### Agregando funcionalidades y estilo (Practico)
* hover sobre imagenes para mayor shadow
* Active en links
* focus en inputs
* focus en inputs requeridos
* Colocando una nueva familia de fuente

** Esta clase no tiene challenge por que los practicos se trabajan sobre el archivo **

## Clase 7 - Flexbox y responsive
### Flexbox
* Explicaciones junto a frogy
### ¿Que es Responsive?
* Mobile first
* Entender unidades absolutas versus unidades relativas
* Media Querys
### Challenge
* Colocar flexbox en nuestro proyecto
* Hacer una imagen responsive
* Haz que la tipografía sea responsive
* Deploy

## Clase 8 -  Introduccion a Bootstrap

## Clase 9 - Challenge HTML - CSS - Bootstrap 4

## Javascript

### Clase 1 : string y numbers
* Que es Js
* Conociendo a la consola
* verificar el tipo de dato con `typeof`
* los valores de tipo number int y float
* operaciones con numeros
    * Sumar dos valores de tipo number
    * Restar dos valores de tipo number
    * Multiplicar dos valores de tipo number
    * Dividir dos valores de tipo number
    * Encontrar un resto en JavaScript
* Crear números decimales con JavaScript
* Multiplica dos decimales con JavaScript
* Divide un decimal por otro con JavaScript

* Declarar variables en js
* La variable **apunta a un valor**
* **Referenciar valores** para una variable
* Iniciando variables con el **operador de asignacion**
* null & undefined
* Comentar codigo en Js
* Entender las variables no inicializadas
* Entendiendo caseSensitive en variables
* Declarar variables de valores de tipo string
* Construyendo strings con variables
* Agregar variables a las strings
* operaciones con datos string
    * Concatenación de strings con operador de suma
    * resta, multiplicacion, y division de string
* Encuentra la longitud de una cadena
* Use la notación de corchetes para encontrar el enésimo carácter en una cadena
* Use la notación de corchetes para encontrar el último carácter en una cadena
* Escapar de citas literales en strings
* Citas de strings con comillas simples
* Secuencias de escape en strings
* Comprender la inmutabilidad de strings
* Word espacios en blanco
* Operaciones entre string y numbers, como pensar en Javascript
* Agregar js al HTML con el tag Script
* Agregar js al HTML con un archivo externo
* console.log()
* alert()
* prompt()

### CLase 2 : booleans, if & else
* Understanding Boolean Values
* Comparison with the Equality Operator
* Comparison with the Strict Equality Operator
* Practice comparing different values
* Comparison with the Inequality Operator
* Comparison with the Strict Inequality Operator
* Comparison with the Greater Than Operator
* Comparison with the Greater Than Or Equal To Operator
* Comparison with the Less Than Operator
* Comparison with the Less Than Or Equal To Operator
* Comparisons with the Logical And Operator
* Comparisons with the Logical Or Operator
* Use Conditional Logic with If Statements
* Introducing Else Statements
* Introducing Else If Statements
* Logical Order in If Else Statements
* Chaining If Else Statements
* Use the Conditional (Ternary) Operator
* Use Multiple Conditional (Ternary) Operators
* pseudocodigo

### Clase 3: while & do while
* Generate Random Fractions with JavaScript
* Generate Random Whole Numbers with JavaScript
* Generate Random Whole Numbers within a Range
* Use the parseInt Function
* Use the parseInt Function with a Radix
* Increment a Number with JavaScript
* Decrement a Number with JavaScript
* Compound Assignment With Augmented Addition
* Compound Assignment With Augmented Subtraction
* Compound Assignment With Augmented Multiplication
* Compound Assignment With Augmented Division
* Iterate with JavaScript While Loops
* Iterate with JavaScript Do...While Loops
* Use Caution When Reinitializing Variables Inside a Loop
* Prevent Infinite Loops with a Valid Terminal Condition

### Clase 4: Valores de tipo function
* Write Reusable JavaScript with Functions
* Passing Values to Functions with Arguments
* Global Scope and Functions
* Local Scope and Functions
* Global vs. Local Scope in Functions
* Return a Value from a Function with Return
* Understanding Undefined Value returned from a Function
* Assignment with a Returned Value
* Returning Boolean Values from Functions
* Return Early Pattern for Functions
* ejercicios

### Clase 5: Que son los arrays?
* Store Multiple Values in one Variable using JavaScript Arrays
* Nest one Array within Another Array
* Access Array Data with Indexes
* Modify Array Data With Indexes
* Access Multi-Dimensional Arrays With Indexes
* Manipulate Arrays With push()
* Manipulate Arrays With pop()
* Manipulate Arrays With shift()
* Manipulate Arrays With unshift()
* Manipulate Arrays With join()
* creating Arrays With split()
* Remove Items Using splice()
* Add Items Using splice()
* Copy Array Items Using slice()
* Check For The Presence of an Element With indexOf()

### Clase 6: Iteradores
Clase practica donde podriamos dar ejercicios que luego seran utilizados en los challenges
* Record Collection
* Iterate with JavaScript For Loops
* Iterate Odd Numbers With a For Loop
* Count Backwards With a For Loop
* Iterate Through an Array with a For Loop
* Iterate Through All an Array's Items Using For Loops
* Nesting For Loops

### Clase 7: Valores de tipo object
* ¿Que es un objeto?
* Creando nuestro primer objeto
* Similitudes de un objeto y un array
* Accediendo al objeto con un bracket notation como al array
* Accediendo al objeto con variables en el Bracket notation como un array
* Accediendo al objeto con dot notation
* Updating Object Properties
* Add New Properties to a JavaScript Object
* Eliminando propiedades con Delete
* Corroborar si un objeto tiene propiedades
* Manipulating Complex Objects
* Accediendo a objetos anidados
* Accediendo a un array de objetos
* Iterando un objeto con for...in
* El metodo Object.keys()
* Modificando un valor array en un objeto
* Creando un metodo del objeto
* El Keyword `this`


### Proyecto de JS / clase con buenas practicas
Esta es una clase donde vamos a tener que hacer utilizando todo lo que se sabe y fortalecer conocimiento.
El ejercicio sera un menu hecho con prompt.
La persona eligira si es usuario o dueño
si es dueño colocara una clave (Ejercicio de preguntar un numero hasta que le pegue)
y va al cambiador de precios
Si es usuario obtiene un menu de 2 tipos de piza y 2 tipos de gaseosa
El coloca el numero del menu o * para terminar
Si el menu fue correcto despliega otro menu que es el de seguir, comprar, o ver el carrito
si elige ver el carrito luego puede ir para atras.


### Manipulando el DOM 
### Eventos
### Proyecto con DOM
Puede ser el menu de un restaurant, el back casi que esta, hay que manipular el dom nomas, mostrando en todo momento el monto y lo que esta comprando.
o este https://codepen.io/TheCodeDepository/pen/jKBaoN

### Jquery
### Ajax & Json
### Proyecto Jquery
un proyecto de clima
https://codepen.io/Melisandre860/pen/RZYqgQ
https://codepen.io/StuffieStephie/pen/QvpxyW?page=4

### Una clase libre










