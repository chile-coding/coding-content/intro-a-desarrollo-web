# Curso Avanzado
Este avanzados es un curso mas fuerte de frontend
##HTML
* Improve Accessibility of Audio Content with the audio Element
* Improve Chart Accessibility with the figure Element
* Wrap Radio Buttons in a fieldset Element for Better Accessibility
* Add an Accessible Date Picker
* Standardize Times with the HTML5 datetime Attribute
* Make Elements Only Visible to a Screen Reader by Using Custom CSS
* Improve Readability with High Contrast Text
* Avoid Colorblindness Issues by Using Sufficient Contrast
* Avoid Colorblindness Issues by Carefully Choosing Colors that Convey Information
* Give Links Meaning by Using Descriptive Link Text
* Make Links Navigatable with HTML Access Keys
* Use tabindex to Add Keyboard Focus to an Element
* Use tabindex to Specify the Order of Keyboard Focus for Several Elements
## CSS
### Primer Clase (Podemos sacarla de FCC)
* Utiliza variables CSS para cambiar varios elementos a la vez
* Crear una variable CSS personalizada
* Usar una variable CSS personalizada
* Adjuntar un valor de reserva a una variable CSS
* Mejora de la compatibilidad con los fallos del navegador
* Como se comportan las variables CSS en cascada
* Cambiar una variable para un área específica
* Usar Media Querys para cambiar una variable
### Segunda Clase 
* Use the CSS Transform scale Property to Change the Size of an Element
* Use the CSS Transform scale Property to Scale an Element on Hover
* Use the CSS Transform Property skewX to Skew an Element Along the X-Axis
* Use the CSS Transform Property skewY to Skew an Element Along the Y-Axis
* Create a Graphic Using CSS
* Create a More Complex Shape Using CSS and HTML
* Learn How the CSS @keyframes and animation Properties Work
* Use CSS Animation to Change the Hover State of a Button
* Modify Fill Mode of an Animation
* Create Movement Using CSS Animation
* Create Visual Direction by Fading an Element from Left to Right
* Animate Elements Continually Using an Infinite Animation Count
* Make a CSS Heartbeat using an Infinite Animation Count
* Animate Elements at Variable Rates
* Animate Multiple Elements at Variable Rates
* Change Animation Timing with Keywords
* Learn How Bezier Curves Work
* Use a Bezier Curve to Move a Graphic
* Make Motion More Natural Using a Bezier Curve
### tercer clase
* Learn about Complementary Colors
* Learn about Tertiary Colors
* Adjust the Color of Various Elements to Complementary Colors
* Adjust the Hue of a Color
* Adjust the Tone of a Color
* Create a Gradual CSS Linear Gradient
* Use a CSS Linear Gradient to Create a Striped Element
* Create Texture by Adding a Subtle Pattern as a Background Image
### Flexbox
### Grid

##JS


# Ecma
* Explore Differences Between the var and let Keywords
* Compare Scopes of the var and let Keywords
* Declare a Read-Only Variable with the const Keyword
* Mutate an Array Declared with const
* Prevent Object Mutation
* Use Arrow Functions to Write Concise Anonymous Functions
* Write Arrow Functions with Parameters
* Write Higher Order Arrow Functions
* Set Default Parameters for Your Functions
* Use the Rest Operator with Function Parameters
* Use the Spread Operator to Evaluate Arrays In-Place
* Use Destructuring Assignment to Assign Variables from Objects
* Use Destructuring Assignment to Assign Variables from Nested Objects
* Use Destructuring Assignment to Assign Variables from Arrays
* Use Destructuring Assignment with the Rest Operator to Reassign Array Elements
* Use Destructuring Assignment to Pass an Object as a Function's Parameters
* Create Strings using Template Literals
* Write Concise Object Literal Declarations Using Simple Fields
* Write Concise Declarative Functions with ES6
* Use class Syntax to Define a Constructor Function
* Use getters and setters to Control Access to an Object
* Understand the Differences Between import and require
* Use export to Reuse a Code Block
* Use * to Import Everything from a File
* Create an Export Fallback with export default
* Import a Default Export
* Selecting from Many Options with Switch Statements
* Adding a Default Option in Switch Statements
* Multiple Identical Options in Switch Statements
* Replacing If Else Chains with Switch
* Use the JavaScript Console to Check the Value of a Variable
* Catch Misspelled Variable and Function Names
* Catch Unclosed Parentheses, Brackets, Braces and Quotes
* Catch Mixed Usage of Single and Double Quotes
* Catch Use of Assignment Operator Instead of Equality Operator
* Catch Missing Open and Closing Parenthesis After a Function Call
* Catch Arguments Passed in the Wrong Order When Calling a Function
* Catch Off By One Errors When Using Indexing
## Array
* Copy an Array with the Spread Operator
* Combine Arrays with the Spread Operator
* Create complex multi-dimensional arrays
## object
Define a Constructor Function
Use a Constructor to Create Objects
Extend Constructors to Receive Arguments
Verify an Object's Constructor with instanceof
Understand Own Properties
Use Prototype Properties to Reduce Duplicate Code
Iterate Over All Properties
Understand the Constructor Property
Change the Prototype to a New Object
Remember to Set the Constructor Property when Changing the Prototype
Understand Where an Object’s Prototype Comes From
Understand the Prototype Chain
Use Inheritance So You Don't Repeat Yourself
Inherit Behaviors from a Supertype
Set the Child's Prototype to an Instance of the Parent
Reset an Inherited Constructor Property
Add Methods After Inheritance
Override Inherited Methods
Use a Mixin to Add Common Behavior Between Unrelated Objects
Use Closure to Protect Properties Within an Object from Being Modified Externally
Understand the Immediately Invoked Function Expression (IIFE)
Use an IIFE to Create a Module

## regular expressions
Using the Test Method
Match Literal Strings
Match a Literal String with Different Possibilities
Ignore Case While Matching
Extract Matches
Find More Than the First Match
Match Anything with Wildcard Period
Match Single Character with Multiple Possibilities
Match Letters of the Alphabet
Match Numbers and Letters of the Alphabet
Match Single Characters Not Specified
Match Characters that Occur One or More Times
Match Characters that Occur Zero or More Times
Find Characters with Lazy Matching
Find One or More Criminals in a Hunt
Match Beginning String Patterns
Match Ending String Patterns
Match All Letters and Numbers
Match Everything But Letters and Numbers
Match All Numbers
Match All Non-Numbers
Restrict Possible Usernames
Match Whitespace
Match Non-Whitespace Characters
Specify Upper and Lower Number of Matches
Specify Only the Lower Number of Matches
Specify Exact Number of Matches
Check for All or None
Positive and Negative Lookahead
Reuse Patterns Using Capture Groups
Use Capture Groups to Search and Replace
Remove Whitespace from Start and End

